import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col} from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {

	const { productId } = useParams();

	// To be able to obtain the user ID so that a user can order
	const { user } = useContext(UserContext);

	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState(0);
	const [ quantity, setQuantity ] = useState(0);

	useEffect(() => {
		console.log(productId);

		// Fetch request that will retrieve the details of the product from the database to be displayed in the "ProductView" page
		fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId]);

	const order = (productId) => {

		fetch(`${ process.env.REACT_APP_API_URL }/users/checkout`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true) {
				Swal.fire({
					title: "Order successful!",
					icon: "success",
					text: "You have successfully ordered the product!"
				})
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Checkout failed."
				})
			}
		})
	}

	const handleDecrement = () => {
		if(quantity > 0){
			setQuantity(quantity - 1)
		}
	}


	const handleIncrement = () => {
		if (user.id !== null && user.isAdmin === false) {
		 	setQuantity(quantity + 1)
		}
	}


	return (

		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3}}>
					<Card border="dark">
						<Card.Body className="text-center">
					        <Card.Title className="mb-3" id="card-title">{name}</Card.Title>
					        <Card.Subtitle id="card-subtitle">Description</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle id="card-subtitle">Price</Card.Subtitle>
					        <Card.Text>Php {price}</Card.Text>
					        <Card.Subtitle id="card-subtitle">Quantity:</Card.Subtitle>
					        <div className="input-group mt-2 mb-3 justify-content-center" >
					        	<Button variant="primary" onClick={() => handleDecrement(productId)} className="input-group-text">-</Button>
					        	<div className="input-group-text text-center">{quantity}</div>
					        	<Button variant="primary" onClick={() => handleIncrement(productId)} className="input-group-text">+</Button>
					        </div>
					        
					        {user.id !== null
					        	?
					        	<Button variant="primary" onClick={() => order(productId)}>Checkout</Button>
					        	:
					        	<Button as={Link} to="/login" variant="danger">Log in to Order</Button>
					    	}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}
